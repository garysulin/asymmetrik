package asymmetrik.challenge;

	import java.io.*;
	import java.util.List;
	import java.awt.*;
	import java.awt.event.*;
	import javax.swing.*;

	/**
	 * Simple Console App to run Mobile Device Keyboard
	 * 
	 * @author gdsulin
	 */
	public class MDKConsole extends WindowAdapter implements WindowListener, 
															 ActionListener{
		private JFrame frame;
		private JTextArea upperTextArea = null;
		private JTextArea lowerTextArea = null;
		private PipedOutputStream pos=new PipedOutputStream();
		
		private AutocompleteProvider provider = null;
		private String passage = "";
		private String candidateReport = "";
		private List<Candidate> candidateList = null;
		private boolean showPrompt = false;

		public MDKConsole(){
			 
			frame=new JFrame("Asymmetrik Mobile Device Keyboard");
		
			Dimension screenSize=Toolkit.getDefaultToolkit().getScreenSize();
			Dimension frameSize=new Dimension((int)(screenSize.width/2),
					(int)(screenSize.height/2));
			int x=(int)(frameSize.width/2);
			int y=(int)(frameSize.height/2);
			frame.setBounds(x,y,frameSize.width,frameSize.height);
			
			upperTextArea=new JTextArea(23, 80);
			upperTextArea.setLineWrap(true);
			upperTextArea.setWrapStyleWord(true);
			upperTextArea.setEditable(true);
			
			lowerTextArea=new JTextArea(5, 80);
			lowerTextArea.setLineWrap(true);
			lowerTextArea.setWrapStyleWord(true);
			lowerTextArea.setEditable(false);
			
			
			JButton button=new JButton("Clear Text");
			
			frame.getContentPane().setLayout(new BorderLayout());
			JPanel textPanel = new JPanel(new BorderLayout());
			
			textPanel.add(new JScrollPane(upperTextArea),BorderLayout.CENTER);
			textPanel.add(new JScrollPane(lowerTextArea),BorderLayout.SOUTH);
			
			frame.getContentPane().add(textPanel,BorderLayout.NORTH);
			frame.getContentPane().add(button,BorderLayout.SOUTH);
			frame.setVisible(true);		
			
			frame.addWindowListener(this);		
			button.addActionListener(this);
			
			showPrompt = true;
			upperTextArea.setText("Begin typing here. Your suggested words "
					+ "will appear in the lower pane. Pressing the "
					+ "'Clear Text' button will clear the visible text "
					+ "but the words previously 'learned' will be retained.");
					
			upperTextArea.addKeyListener(new KeyListener() {

				public void keyPressed(KeyEvent e) {}
				public void keyReleased(KeyEvent e) {}

				public void keyTyped(KeyEvent e)  {
					
					if(showPrompt){
						showPrompt = false;
						upperTextArea.setText("");
					}
					
					char typedChar = e.getKeyChar();
					
					try { 
						pos.write(e.getKeyChar()); 
					} catch (IOException ex) {}
			
					// If user completes a word, 'train' it:
					if(typedChar == ' ' || typedChar == '\n'){
					
						provider.train(passage);
						lowerTextArea.setText("");
						passage = "";
					
					// user is still typing, make suggestions:
					} else {
						
						// oops! dumb-thumb
						if(typedChar == '\b'){
							if(passage.length() > 0){
								passage = passage.substring(
										0, passage.length() - 1);
							}
						} else {
							passage += typedChar;
						}
						candidateList = provider.getWords(
												passage.toLowerCase());
						candidateReport = "";
						if(candidateList != null){
							for(Candidate candidate: candidateList){
								candidateReport += "\"" + candidate.getWord() 
								+ "\" (" + candidate.getConfidence() + "),";
							
							}
							lowerTextArea.setText(candidateReport); 
						} else {
							lowerTextArea.setText("");
						}
					}
						
				}});
			
			provider = new AutocompleteProvider();
		}
		
		public synchronized void windowClosed(WindowEvent evt){
			this.notifyAll();  
			try { 
				pos.close(); 
			} catch (Exception e){}  
			System.exit(0);
		}		
			
		public synchronized void windowClosing(WindowEvent evt){
			frame.dispose();
		}
		
		public synchronized void actionPerformed(ActionEvent evt){
			this.clear();
		}

		public void clear(){
			upperTextArea.setText("");
			lowerTextArea.setText("");
			passage = "";
		}
		
		 public static void main(String[] args){
			    
		    new MDKConsole();
	    }

}
