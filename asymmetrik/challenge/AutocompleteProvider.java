package asymmetrik.challenge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Learns the words typed by user over time and determines a ranked list of 
 * autocomplete candidates given a word fragment.
 * 
 * @author gdsulin
 */
public class AutocompleteProvider {
	
	/**
	 * A HashMap whose keys are word fragments, and whose value for each key is 
	 * a list of Candidate object references. The Candidate objects each 
	 * contain a word that could be the user's intended typed word, and its 
	 * corresponding confidence score.
	 */
	private HashMap<String, ArrayList<Candidate>> fragmentMap = null;
	
	/**
	 * A HashMap whose keys are words that have already been 'trained' at least
	 * once, and whose value for each key is its corresponding Candidate. This 
	 * allows us to quickly determine if a Candidate already exists for a given
	 * word (and increase its confidence score), and to store the Candidates in
	 * a single place. We can then use refs to these objects in our fragmentMap
	 * instead of multiple Candidate objects for a word.
	 */
	private HashMap<String, Candidate> candidateMasterList = null;
	
	public AutocompleteProvider(){
		
		fragmentMap = new HashMap<String, ArrayList<Candidate>>();
		candidateMasterList = new HashMap<String, Candidate>();
		
	}
	
	/**
	 * Returns a list of candidate words and their respective confidence 
	 * scores.
	 * 
	 * @param fragment - incomplete word being typed at the moment.
	 * @return List of Candidates, or null if no Candidate corresponds to 
	 * fragment.
	 */
	public List<Candidate> getWords(String fragment){
		
		List<Candidate> result = fragmentMap.get(fragment);
		
		if(result != null){
			Collections.sort(result);
		}
		
		return result;
		
	} // end getWords

	/**
	 * As the typing of each word is completed (the user types a 'space' or 
	 * 'enter'), either store the word in our structure, or increment its 
	 * score if it's already there.
	 * 
	 * @param passage - word to be stored/scored
	 */
	public void train(String passage){
		
		int wordLength = passage.length();
		
		if(wordLength > 1){
	
			passage = passage.toLowerCase();
			Candidate newCandidate = null;
			
			/* 
			 * First see if we have a Candidate for this passage, if so bump 
			 * its score, if not create it:
			 */
			if(candidateMasterList.containsKey(passage)){
				
				candidateMasterList.get(passage).incrementConfidence();
				
			} else {
				
				newCandidate = new Candidate(passage);
				candidateMasterList.put(passage, newCandidate);
				
				/*
				 *  Now make sure passage's Candidate is linked to each 
				 *  possible fragment:
				 */
				int index = 2;
				String key = "";
				ArrayList<Candidate> candidates = null;
				
				for(index = 2; index < wordLength; index++){
					
					/*
					 * i.e. for the word 'treat', key is 'tr', then 'tre', 
					 * then 'trea'
					 */
					key = passage.substring(0, index);
					
					/*
					 * If the map contains this key, see if passage is already
					 * mapped to it:
					 */
					if(fragmentMap.containsKey(key)){
						candidates = fragmentMap.get(key);
						
						boolean foundIt = false;
						
						/*
						 *  If passage is already mapped to this key, we can 
						 *  move on:
						 */
						for(Candidate thisCandidate : candidates){
							
							if(thisCandidate.getWord().equals(passage)){
								foundIt = true;
								break;
							}
						} // end inner 'for'
						
						if(!foundIt){
							
							/*
							 *  passage not yet mapped to this key, so let's 
							 *  do it now:
							 */
							candidates.add(newCandidate);
						}
						
					/*
					 *  If this key is not yet in the map, add it, and map passage 
					 *  to it:
					 */
					} else {
						
						ArrayList<Candidate> newCandidates = 
													new ArrayList<Candidate>();
						newCandidates.add(newCandidate);
						fragmentMap.put(key, newCandidates);
						
					} // end 'if/else'
						
				} // end outer 'for'
				
			} // end if master list contains key
			
		} // end if length > 1
		
	} // end train
}
