package asymmetrik.challenge;

/** 
 * Contains a word that has been 'trained' in our app and its confidence,
 * which equals the number of times the word has typed by the user.
 * @author gdsulin
 */
public class Candidate implements Comparable<Object>{

		private String word = "";
		
		/** Confidence score: */
		private Integer countInt = null;
		
		public Candidate(String value){
			word = value;
			countInt = new Integer(1);
		}
		
		public String getWord(){
			return word;
		}
		
		public Integer getConfidence(){
			return countInt;
		}
		
		public void incrementConfidence(){
			countInt = new Integer(countInt.intValue() + 1);
		}
		
		/** Implement Comparable Interface: */
		public int compareTo(Object o){
			return ((Candidate)o).getConfidence().
					compareTo(this.getConfidence());
		}
}
