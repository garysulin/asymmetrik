
Mobile Device Keyboard App

To build: 
	in this directory, execute 'build_me.bat'

To run:
	in this directory, execute 'run_me.bat'

User instructions will appear on the app window, and will disappear with the first typed keystroke.

